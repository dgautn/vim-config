> Repositorio antiguo. Todas las configuraciones actuales en [dot_files](https://gitlab.com/dgautn/dot_files.git)

# Configuraciones de Vim

#### [Packs incluidos](packs_includ.txt 'tree pack/ -d -L 4 > packs_includ.txt')

---
[fugitive](https://tpope.io/vim/fugitive.git)

[fugitive-gitlab.vim](https://github.com/shumphrey/fugitive-gitlab.vim.git)

[jedi-vim](https://github.com/davidhalter/jedi-vim.git)

[markdown-preview](https://github.com/iamcco/markdown-preview.vim)

[supertab](https://github.com/ervandew/supertab.git)

[vim-cisco-ios](https://github.com/CyCoreSystems/vim-cisco-ios)

[vim-openscad](https://github.com/sirtaj/vim-openscad.git)

[dracula-vim](https://github.com/dracula/vim/)

[nerdtree](https://github.com/preservim/nerdtree.git)

[nerdtree-git-plugin](https://github.com/Xuyuanp/nerdtree-git-plugin.git)

[clang_complete](https://github.com/xavierd/clang_complete.git)

---

### Recargar configuración
``` :source $MYVIMRC ```

### Administarción de complementos sin *plugin-managers*

Ubicación de los complementos

``` ~/.vim/pack/plugins/start/ ```

Listar los complementos

``` :scriptnames ``` 

Cargar el complemento (e.g. **foo**)

``` :packadd foo ``` 

Para agregar documentación del complemento

``` :helptags ~/.vim/pack/plugins/start/foo ``` 

o generar documentacion para todos los complementos en *runtimepath* de Vim

``` :helptags ALL ``` 

Eliminar un complemento

``` rm -r ~/.vim/pack/plugins/start/foo ``` 
